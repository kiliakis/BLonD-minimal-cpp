/*
 * configuration.h
 *
 *  Created on: Mar 8, 2016
 *      Author: kiliakis
 */

#ifndef INCLUDES_CONFIGURATION_H_
#define INCLUDES_CONFIGURATION_H_

typedef double ftype;

// Compile time switches
// Needs re-compile every time a change is made

//switch on/off fixed beam distribution
#define FIXED_PARTICLES

// switch on/off timing
#define TIMING

// switch on/off result printing
#define PRINT_RESULTS

#endif /* INCLUDES_CONFIGURATION_H_ */
